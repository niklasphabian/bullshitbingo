import random
import math
import os

class Wordlist():
    def __init__(self):
        self.wordlist = []
    
    def load_dictionary(self):
        with open('words.txt', 'r') as wordfile:    
            for line in wordfile:        
                self.wordlist.append(line[0:-1])
                
    def draw_word(self):
        idx = int(math.floor(random.random() * len(self.wordlist)))
        return self.wordlist.pop(idx)

    def write_random_parameter_file(self, paramert_filename):
        self.load_dictionary()
        with open(paramert_filename, 'w') as parfile:
            for n in range(1,26):
                idx = int(math.floor(random.random() * len(self.wordlist)))
                word = self.wordlist.pop(idx)
                parfile.write(word)
                if n%5 == 0 :
                    parfile.write('\n')
                else :
                    parfile.write(', \t')
                n += 1   
                
def make_pdf(outname):
    os.system('cp bingo.tex {target}.tex'.format(target=outname))
    wl = Wordlist()
    wl.write_random_parameter_file('tab_1.csv')
    wl.write_random_parameter_file('tab_2.csv')
    wl.write_random_parameter_file('tab_3.csv')
    os.system('pdflatex {target}.tex'.format(target=outname))
    os.system('mv {target}.pdf bingo_fields/{target}.pdf'.format(target=outname))
    os.system('rm {target}.*'.format(target=outname))

for n in range(1,5):
    fname = 'bingo_{}'.format(n)
    make_pdf(fname)